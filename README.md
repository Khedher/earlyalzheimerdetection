# EarlyAlzheimerDetection

Machine learning has a phenomenal range of application in the health sciences. This tutorial will go over the complete pipeline to build a model that can determine the dementia level of an Alzheimer's patient from their MRI image.

This tutorial highlights the ease of building a CNN using tf.keras. Additionally, TensorFlow 2.3 (or over) has new features, including easy data loading utilities that were previously not available in TensorFlow 2.2. We'll be seeing how easy data loading is with these additional features.

We'll be using a GPU accelerator for this notebook.

This notebook includes the following main steps:


1. Upload data to colab (.zip file)
2. Import librairies
3. Data loading
4. Visualize data
5. Feature Engineering
6. Build the ML Model
7. Training the Model
8. Visualize Model Metrics
9. Model Evaluation
10. Evaluate a pretrained model 
